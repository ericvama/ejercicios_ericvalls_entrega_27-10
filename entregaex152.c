//15. Write a program that asks for 1 number to the user and show its binary equivalent value. You can show the binary in inverted order.


/*Este programa lo he intentado hacer para que muestre el numero binario en el orden correcto. Lo he intentado de todas las maneras pero creo que esta es la mas acertada aunque no funcione
a la perfeccion. Es decir que el resultado que muestra es correcto, lo unico que delante de este me salen numeros "aleatorios" y no entiendo por que. 
Me gustaria que me lo explicaras cuando lo hayas corregido. Muchas gracias*/


#include <stdio.h> // Introducir libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    int num; // Declarar la variable entera "num"
    int num2 = num; // Declarar la variable entera "num2" y determinarle el valor de la variable "num"
    int residuo; // Declarar la variable entera "residuo"
    int n[50]; // Declarar un array de 50 numeros decimales, ya que no sabemos cuantos bits ocupa el numero que introduce el usuario
    int cont = 0; // Declarar la variable entera "cont" y determinarle el valor 0
    
    printf("Introduce un numero entero: "); // Pedir al usuario que introduzca un numero
    scanf("%d",&num); // Determinar el valor introducido a la variable "num"
    getchar();
    
    while( num <= 0) // Si el numero introducido es negativo o igual a 0, volver a pedir un valor hasta que este sea positivo y determinarle a "num"
    {
        printf("Introduzca un numero entero positivo: ");
        
        scanf("%d",&num);
        getchar();
    }
    
    int num3 = num; // Declarar la variable entera "num3" y determinarle el valor de la variable "num"
    
    
    while( num3 > 0) // Funcion para determinar cada bit en orden invertido en el array
    {
        residuo = num2 % 2;
    
        num3 /= 2;
        num2 = num3;
        
        n[cont] = residuo;
        cont ++;
    }
    
    for( int i = cont; i >= 0; i --) // Funcion para leer el array en orden invertido, mostrando asi, la posicion correcta de los bits
    {
        printf("%d", n[i]);
    }
    
    getchar();
    return (0); // Devolver 0. (Final del programa)
}