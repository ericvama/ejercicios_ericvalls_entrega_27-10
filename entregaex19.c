//19. Write a function to calculate power of a value. Function declaration should be: int RaiseToPower(int value, int power);

#include <stdio.h> // Introducir libreria "stdio.h"

int RaiseToPower(float value, int power) // Declarar la variable entera "RaiseToPower" que nos mostraran la potencia de "power" de "value"
{
    float result = 1; // Declarar el resultado determinandole el valor 1 para empezar
    
    for (int i = power; i > 0; i --) // Multiplicar el valor introducido en "num" al resultado el numero de veces introducidos en "pot"
    {
        result *= value;
    }
    
    printf("\n%.3f elevado a %d es: %.3f", value, power, result); // Mostrar en la pantalla lo que esta escrito entre " "
}

// Punto de entrada del programa
int main()
{
    float num; // Declarar la variable decimal "num"
    int pot; // Declarar la variable entera "pot"
    
    printf("Introduzca un valor que quiera potenciar: "); // Pedir al usuario el valor que quiera potenciar
    
    scanf("%f",&num); // Determinar el valor introducido a la variable "num"
    getchar();
    
    printf("\nIntroduzca el valor de la potencia: "); // Pedir al usuario el valor de la potencia
    
    scanf("%d",&pot); // Determinar el valor introducido a la variable "pot"
    getchar();
    
    int calcularpotencia = RaiseToPower(num, pot); // Determinar la variable "RaiseToPower" a la variable "calcularpotencia"
        
    getchar();
    return (0); // Devolver 0. (Final del programa)
}