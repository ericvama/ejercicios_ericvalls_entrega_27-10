//13. Write a program that asks for a sequence of numbers to the user and show the sum of all of them. Process stops when a negative number is introduced.

#include <stdio.h> // Introducir libreria "stdio.h"

// Punto de entrada del programa
int main(){
    
    printf("Programa para calcular la suma de una secuancia de numeros\n"); // Mostrar en la pantalla la explicacion del programa
    printf("Introducir un numero negativo para calcular la suma total\n\n");
    
    printf("Dame un numero positivo: "); // Pedir un numero positivo
    
    float num1; // Declarar la variable decimal "num1"
    
    scanf("%f",&num1); //Determinar valor introducido a "num1"
    getchar();
    
    float suma = 0; // Declarar la variable decimal "suma"
    
    while (num1 >= 0) // Mientras los numeros introducidos sean positivos, se van sumando al valor de la variable "suma"
    {
        suma += num1;
        
        printf("Dame otro numero:");
        
        scanf("%f",&num1);
        getchar();
    }

    printf("\nLa suma de todos los numeros positivos es: %.4f", suma); // Al introducir un numero negativo salimos del bucle y nos da el resultado
    
    getchar();
    return (0); // Devolver 0. (Final del programa)
}