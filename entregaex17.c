//17. Write a program that prints five random numbers, like a Lottery.

#include <stdio.h> // Introducir libreria "stdio.h" y "stdlib.h"
#include <stdlib.h>

// Punto de entrada del programa
int main()
{
    time_t t; // Declarar variable "time_t" y determinarlo como "t"
    
    srand((unsigned) time(&t));
    
    for( int i = 0; i < 5; i ++ ) // Mostrar en la pantalla 5 numeros aleatorios entre el 0 y el 100 mediante un bucle
    {
        printf("%d\n", rand()%100);
    }
    
    getchar();
    return (0); // Devolver 0. (Final del programa)
}