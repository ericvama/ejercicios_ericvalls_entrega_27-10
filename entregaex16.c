//16. Write a program that that displays the first 100 prime numbers.

#include <stdio.h> // Introducir libreria "stdio.h" y "stdlib.h"
#include <stdlib.h>

// Punto de entrada del programa
int main()
{

int cont = 0; // Declarar la variable entera "cont" y determinarle el valor 0
int salirbucle; // Declarar la variable entera "salirbucle"
int i; // Declarar la variable entera "i"

printf("Los 100 pimeros numeros primos son: "); // Mostrar en la pantalla lo que esta entre " "

for( int j = 2; cont < 100; j ++) // Bucle para generar numeros. Cuando "cont" sea = 100, es decir que tengamos 100 num primos, se sale del bucle y acaba programa
{
    salirbucle = 0;
    i = 2;
    
    while (( i <= j ) && ( salirbucle == 0)) // Bucle para saber si un numero "j" es positivo o no. En caso de que lo sea se mostrara el numero en pantalla y sumar 1 a "cont", si no se sale del bucle y se le suma 1
    {
        if (( j%i == 0 ) && ( i == j ))
        {
            printf("%d, ", j);
            cont ++;
        }
        else if ( j%i == 0 )
        {
            salirbucle = 1;
        }
        
        i ++;
    }
}

getchar ();
return 0; // Devolver 0. (Final del programa)
}