//05. Write a program that asks for radius of a circle and find its diameter, circumference and area.

#include <stdio.h> // Importar libreria "stdio.h"

// Punto de entrada del programa
int main()
{
    float radio; // Declarar una variable de tipo decimal nombrada "radio"
    
    printf("Escribe el radio de una circunferencia: "); // Mostrar en la pantalla el texto escrito entre " "

    scanf ("%f",&radio); // Determinar el valor introducido a la variable "radio"
    getchar(); // Dar al espacio para continuar
  
    printf("\nEl diametro de la circunferencia es: %.3f\n", radio * 2); // Mostrar el texto entre " ", sustituyendo "%f" por la operacion que aparece despues de la coma
    printf("El perimetro de la circunferencia es: %f\n", radio * 2 * 3.14);
    printf("La area de la circunferencia es: %5.5f\n", radio * radio * 3.14);
    
    getchar();
    return 0; // Devolver 0. (Final del programa)
}